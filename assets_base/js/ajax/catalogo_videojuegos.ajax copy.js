function pruebaJS() {
   var myHeaders = new Headers();
   myHeaders.append("Client-ID", "efbd78h8gkhku20i2assug435n3mpl");
   myHeaders.append("Authorization", "Bearer 18rhqm29xcioje56pj9s7v1lcgcvia");
   // myHeaders.append("Content-Type", "text/plain");
   myHeaders.append("Content-Type", "application/json");
   myHeaders.append("Access-Control-Allow-Headers", "*");
   myHeaders.append(
      "Access-Control-Allow-Methods",
      "GET,PUT,POST,DELETE,OPTIONS"
   );
   myHeaders.append("Access-Control-Allow-Origin", "*");

   var raw =
      "fields  id, name, platforms.name,genres.name, screenshots.image_id, cover.image_id, collection.games.name, collection.games.cover.image_id, collection.games.platforms.name, game_modes.name, themes.name, videos.video_id, involved_companies.company.name, franchises.name, franchises.games.name, franchises.games.platforms.name, similar_games.name, similar_games.cover.image_id, similar_games.platforms.name, release_dates.region, release_dates.y, release_dates.m, summary;\r\nwhere platforms = 130;\r\nlimit 5;";

   var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
   };

   fetch(
      "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games/",
      requestOptions
   )
      .then((response) => {
         // console.log("ANDRES PRUEBA Response", response.json());

         response.json().then((dato) => {
            for (var i in dato) {
               console.log("Hola: ", dato[i].id);
            }
         });
      })
      .catch((error) => console.log("error", error));
}

function obtCatalogoVideojuegos() {
   $("#formCatalogoVideojuegos").submit((e) => {
      e.preventDefault();

      var myHeaders = new Headers();
      myHeaders.append("Client-ID", "efbd78h8gkhku20i2assug435n3mpl");
      myHeaders.append(
         "Authorization",
         "Bearer 18rhqm29xcioje56pj9s7v1lcgcvia"
      );
      // myHeaders.append("Content-Type", "text/plain");
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Access-Control-Allow-Headers", "*");
      myHeaders.append(
         "Access-Control-Allow-Methods",
         "GET,PUT,POST,DELETE,OPTIONS"
      );
      myHeaders.append("Access-Control-Allow-Origin", "*");

      const postData = {
         function: "catalogoVideojuegos",
         search: $("#txtBusquedaJuego").val(),
         limit: $("#selLimite").val(),
         platform: $("#selPlataformas option:selected").val(),
      };

      const url = "controladores/videojuegos.controlador.php";
      // console.log(postData, url);
      $.post(url, postData, (response) => {
         $("#tblCatalogoVideojuegos tbody tr").remove();
         var t = "";

         var nintendoPlatformsArray = [
            4, 5, 18, 19, 20, 21, 22, 24, 33, 37, 41, 130, 137, 159,
         ];
         var sonyPlatformsArray = [7, 8, 9, 38, 46, 48, 167];
         var microsoftPlatformsArray = [11, 12, 49, 169];
         var bgNintendo = "bg-nintendo";
         var bgSony = "bg-sony";
         var bgMicrosoft = "bg-microsoft";

         console.log(response);
         $.each(JSON.parse(response), function (i, item) {
            var tr = "";
            tr += "<tr>";
            tr += "<td><div class='form-check'>";
            tr +=
               "<input type='checkbox' class='form-check-input' id='customCheck2'>";
            tr +=
               "<label class='form-check-label' for='customCheck2'></label></td>";
            tr += "</div>";
            // --------------- ID
            // tr += "<td class='align-middle'>" + item.gameId + "</td>";
            // --------------- Image url
            tr +=
               "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src=" +
               item.gameUrlCoverImage +
               "></div></td>";
            // --------------- Nombre
            tr += "<td class='align-middle'>" + item.gameName + "</td>";
            // --------------- Consolas
            tr += "<td class='align-middle'>";

            for (var x = 0; x < item.gamePlatform.length; x++) {
               tr +=
                  "<span class='badge " +
                  item.gamePlatform[x]["classPlatform"] +
                  " mx-1'>" +
                  item.gamePlatform[x]["abbrevNamePlatform"] +
                  " </span>";
            }
            tr += "</td>";
            // --------------- Generos
            tr += "<td class='align-middle'>";
            for (var x = 0; x < item.gameGenre.length; x++) {
               tr +=
                  "<span class='badge badge-warning-lighten mx-1'>" +
                  item.gameGenre[x]["nameGenre"] +
                  " </span>";
            }
            tr += "</td>";
            // --------------- Modos de juego
            tr += "<td class='align-middle'>";
            for (var x = 0; x < item.gameGameMode.length; x++) {
               tr +=
                  "<span class='badge badge-info-lighten mx-1'>" +
                  item.gameGameMode[x]["nameGameMode"] +
                  " </span>";
            }
            tr += "</td>";
            tr += "</tr>";
            t += tr;
         });
         document.getElementById("tblCatalogoVideojuegosBody").innerHTML += t;
      });
   });
   return false;
}

function obtListadoPlataformas() {
   const listadoPlataformasData = {
      function: "listadoPlataformas",
   };

   const url = "controladores/videojuegos.controlador.php";
   $.post(url, listadoPlataformasData, (response) => {
      console.log(response);
      var codListaNintendo = "";
      var sumN = "";
      var codListaSony = "";
      var sumS = "";
      var codListaMicrosoft = "";
      var sumX = "";

      $("#tblListaNintendo tbody tr").remove();
      $("#tblListaSony tbody tr").remove();
      $("#tblListaMicrosoft tbody tr").remove();

      $.each(JSON.parse(response), function (i, item) {
         if (item.platformFamily == 5) {
            sumN += "<tr>";
            sumN += "<td>" + item.platformId + "</td>";
            sumN += "<td>" + item.platformAbbrev + "</td>";
            sumN += "</tr>";
         }

         if (item.platformFamily == 1) {
            sumS += "<tr>";
            sumS += "<td>" + item.platformId + "</td>";
            sumS += "<td>" + item.platformAbbrev + "</td>";
            sumS += "</tr>";
         }

         if (item.platformFamily == 2) {
            sumX += "<tr>";
            sumX += "<td>" + item.platformId + "</td>";
            sumX += "<td>" + item.platformAbbrev + "</td>";
            sumX += "</tr>";
         }
      });

      codListaNintendo += sumN;
      codListaSony += sumS;
      codListaMicrosoft += sumX;

      document.getElementById("tblListaNintendoBody").innerHTML +=
         codListaNintendo;
      document.getElementById("tblListaSonyBody").innerHTML += codListaSony;
      document.getElementById("tblListaMicrosoftBody").innerHTML +=
         codListaMicrosoft;
   });
   return false;
}

$(document).ready(function () {
   // obtListadoPlataformas();
   // obtCatalogoVideojuegos();
   pruebaJS();
});
