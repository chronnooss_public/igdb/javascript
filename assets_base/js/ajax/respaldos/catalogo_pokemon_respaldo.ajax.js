const getRandomInt = (min, max) => {
   return Math.floor(Math.random() * (max - min)) + min;
};

console.log(getRandomInt(1, 151));

document.addEventListener("DOMContentLoaded", () => {
   const random = getRandomInt(1, 151);
   fetchData(random);
});

// https://pokeapi.co/docs/v2#pokemon
// https://pokeapi.co/
// https://documenter.getpostman.com/view/10670805/SzS2xToN

const fetchData = async (id) => {
   try {
      $("#tblCatalogoPokedex tbody tr").remove();
      var t = "";
      for (var k = 1; k <= 898; k++) {
         var tr = "";
         const res = await fetch("https://pokeapi.co/api/v2/pokemon/" + k);
         const data = await res.json();

         tr += "<tr>";
         tr += "<td class='align-middle'>" + data.id + "</td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" +
            k +
            ".png></div></td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img style='width: 100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.name + "</td>";
         tr += "<td class='align-middle'>";
         $.each(data.types, function (i, item) {
            tr +=
               "<span class='badge bg-" +
               item.type.name +
               " mx-1'>" +
               item.type.name +
               " </span>";
         });
         tr += "</td>";
         tr +=
            "<td class='align-middle'> <div class='row'> <img  class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/" +
            k +
            ".png></div></td>";
         tr += "<td class='align-middle'>" + data.stats[0].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[1].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[2].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[3].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[4].base_stat + "</td>";
         tr += "<td class='align-middle'>" + data.stats[5].base_stat + "</td>";
         tr += "</tr>";
         t += tr;
      }
      document.getElementById("tblCatalogoPokedexBody").innerHTML += t;
   } catch (error) {
      console.log(error);
   }
};

// function pokeApi() {
//    var settings = {
//       url: "https://pokeapi.co/api/v2/pokemon/",
//       method: "GET",
//       timeout: 0,
//    };

//     const fetchData = async (id) => {
//         try {
//             const res = await fetch('https://pokeapi.co/api/v2/pokemon/${id}')
//             const data = await res.json();

//             const pokemon = { pkm: pokemon.name }

//        }
//    }

//    $.ajax(settings).done(function (response) {
//       //    console.log(response);

//       $.each(JSON.parse(response), function (i, item) {
//          console.log(item.name);
//       });
//    });
// }

// $(document).ready(function () {
//    pokeApi();
// });
