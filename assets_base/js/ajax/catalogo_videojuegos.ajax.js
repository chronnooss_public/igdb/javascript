$(document).ready(function () {
   obtListaVideojuegos();
});

function obtListaVideojuegos() {
   $("#formCatalogoVideojuegos").submit(async (e) => {
      e.preventDefault();

      // Variables
      var trConcatenado = "";
      var nintendoPlatformsArray = [
         4, 5, 18, 19, 20, 21, 22, 24, 33, 37, 41, 130, 137, 159,
      ];
      var sonyPlatformsArray = [7, 8, 9, 38, 46, 48, 167];
      var microsoftPlatformsArray = [11, 12, 49, 169];
      var bgNintendo = "bg-nintendo";
      var bgSony = "bg-sony";
      var bgMicrosoft = "bg-microsoft";
      // ******** ini Api Token
      var requestOptionsTokenApi = {
         method: "POST",
         mode: "cors",
      };
      const response = await fetch(
         "https://id.twitch.tv/oauth2/token?client_id=efbd78h8gkhku20i2assug435n3mpl&client_secret=bxl2j2k6rjocazgncxjxl7onqum4pl&grant_type=client_credentials",
         requestOptionsTokenApi
      );
      const responseText = await response.json();
      var accessToken = "Bearer " + responseText.access_token;

      // ******** fin Api Token

      // ******** ini Api IGDB games

      var search = $("#txtBusquedaJuego").val();
      var limit = $("#selLimite").val();
      var platform = $("#selPlataformas option:selected").val();
      // var raw ='fields  id, name, platforms.name,genres.name, screenshots.image_id, cover.image_id, collection.games.name, collection.games.cover.image_id, collection.games.platforms.name, game_modes.name, themes.name, videos.video_id, involved_companies.company.name, franchises.name, franchises.games.name, franchises.games.platforms.name, similar_games.name, similar_games.cover.image_id, similar_games.platforms.name, release_dates.region, release_dates.y, release_dates.m, summary;\r\nsearch "mario bros";\r\nwhere platforms = 130;\r\nlimit 5;';
      var raw =
         "fields  id, name, platforms.name,genres.name, screenshots.image_id, cover.image_id, collection.games.name, collection.games.cover.image_id, collection.games.platforms.name, game_modes.name, themes.name, videos.video_id, involved_companies.company.name, franchises.name, franchises.games.name, franchises.games.platforms.name, similar_games.name, similar_games.cover.image_id, similar_games.platforms.name, release_dates.region, release_dates.y, release_dates.m, summary;";
      raw += '\r\nsearch "mario bros";';
      raw += "\r\nwhere platforms = 130;";
      raw += "\r\nlimit " + limit + ";";
      // console.log("raw -> " + raw);

      var myHeaders = new Headers();
      myHeaders.append("Client-ID", "efbd78h8gkhku20i2assug435n3mpl");
      myHeaders.append("Authorization", accessToken);
      myHeaders.append("Content-Type", "application/json");

      var requestOptions = {
         method: "POST",
         headers: myHeaders,
         body: raw,
         redirect: "follow",
      };

      fetch(
         "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games/",
         requestOptions
      )
         .then((response) => {
            $("#tblCatalogoVideojuegos tbody tr").remove();

            response.json().then((dato) => {
               for (var i in dato) {
                  trConcatenado = `
                        <tr>
                           <td>
                              <div class='form-check'>
                                 <input type='checkbox' class='form-check-input' id='customCheck2'>
                                 <label class='form-check-label' for='customCheck2'></label>
                              </div>
                           </td>

                           <td class='align-middle'>
                              <div class='row'>
                                 <img style='width: 100px;' class='rounded mx-auto d-block' src="https://images.igdb.com/igdb/image/upload/t_cover_big/${dato[i].cover.image_id}.jpg">
                              </div>
                           </td>

                           <td class='align-middle'>${dato[i].name}</td>
                     `;

                  trConcatenado += `<td class='align-middle'>`;
                  for (var x = 0; x < dato[i].platforms.length; x++) {
                     trConcatenado += `<span class='badge bg-primary mx-1'>${dato[i].platforms[x].name}</span>`;
                  }
                  trConcatenado += `</td>`;
                  // trConcatenado += `
                  //                      <td class='align-middle'>
                  //                         <span class='badge bg-nintendomx-1'>{abbrevNamePlatform}</span>
                  //                      </td>
                  //                   `;
                  trConcatenado += `<td class='align-middle'>`;
                  for (var x = 0; x < dato[i].genres.length; x++) {
                     trConcatenado += `<span class='badge badge-warning-lighten mx-1'>${dato[i].genres[x].name}</span>`;
                  }
                  trConcatenado += `</td>`;
                  // trConcatenado += `
                  //                      <td class='align-middle'>
                  //                         <span class='badge badge-warning-lighten mx-1'>{nameGenre}</span>
                  //                      </td>
                  //                   `;
                  trConcatenado += `<td class='align-middle'>`;
                  for (var x = 0; x < dato[i].game_modes.length; x++) {
                     trConcatenado += `<span class='badge badge-info-lighten mx-1'>${dato[i].game_modes[x].name}</span>`;
                  }
                  trConcatenado += `</td>`;
                  // trConcatenado += `
                  //                      <td class='align-middle'>
                  //                         <span class='badge badge-info-lighten mx-1'>{nameGameMode}</span>
                  //                      </td>
                  //                   `;

                  trConcatenado += `</tr>`;

                  // console.log(trConcatenado);
               }
               document.getElementById(
                  "tblCatalogoVideojuegosBody"
               ).innerHTML += trConcatenado;
            });
         })
         .catch((error) => console.log("error", error));
      // ******** fin Api IGDB games
   });
}
